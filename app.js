'use strict';
const express = require('express');
const Moment = require('moment');
const bodyParser = require("body-parser");
const mysql = require("mysql2");
const mail = require('./mail.js');
const fs = require('fs');
const ejs = require('ejs');
const https = require('https');
const multer  = require('multer');
const upload = multer({dest: 'uploads/'});
const AWS = require('aws-sdk');
const s3 = new AWS.S3({
    accessKeyId: 'AKIAIHG3GKELJRKJYDFA',
    secretAccessKey: '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'
});


const emailFrom = 'KonnectShift <info@konnectshift.com>';
const emailFromShyftLabs = 'ShyftLabs <info@shyftlabs.io>';
const emailFromWeb = 'web@konnectshift.com';
const emailFromWebLabs = 'web@shyftlabs.io';
const subRequest = 'Demo Requested for KonnectShift';
const subCareer = 'Career Application for KonnectShift';
const subContactLabs = 'Contact Us for ShyftLabs';
const subContact = 'Contact Us for KonnectShift';
const subRequestToUser = 'KonnectShift Demo Requested';
const subContactToUser = 'Thank you very much for reaching us out!';


const sql = mysql.createPool({
    host: "websitesql.cgcp3pczug27.ap-south-1.rds.amazonaws.com",
    user: "websitemaster",
    password: "qwertyzxcvbnm",
    database: "konnect",
    connectionLimit: 100
});


const port = process.env.PORT || 3000;


const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));

// parse application/json
app.use(bodyParser.json());

const options = {
    key: fs.readFileSync('/etc/letsencrypt/live/konnectshift.com/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/konnectshift.com/cert.pem'),
};

const server = https.createServer(options, app).listen(port, function () {
    console.log("Express server listening on port " + port);
});

// Add headers
app.use(function (req, res, next) {

    const origin = req.headers.origin;

    if (origin === 'https://shyftlabs.io' || origin === 'http://13.57.234.34' || origin === 'http://shyftlabs.io' || origin === 'https://www.shyftlabs.io') {
        res.setHeader('Access-Control-Allow-Origin', origin);
    } else {
        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', 'https://konnectshift.com');
    }
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.get('/', (req, res) => {
    return res.status(200).json({message: 'KonnectShift!'});
});

app.post('/contact', function (req, res) {
    try {

        console.log(req.body);

        const user = {
            name: req.body.name ? req.body.name : '',
            organisation: req.body.organisation ? req.body.organisation : '',
            mobile_number: req.body.mobile_number ? req.body.mobile_number : '',
            email: req.body.email ? req.body.email : '',
            description: req.body.description ? req.body.description : '',
            position: req.body.position ? req.body.position : '',
            fleet_size: req.body.fleet_size ? req.body.fleet_size : '',
            request_type: req.body.request_type ? req.body.request_type : '',
            status: 'Contacted',
            source: 'KonnectShift',
            requested_at: Moment.utc().format('YYYY-MM-DD HH:mm:ss')
        };

        const contentRequest = 'Hi Team, ' + user['name'] + '\n\n just contacted us for KonnectShift.' + '\n\n' +
            'Name : ' + user['name'] + '\n\n' +
            'Organisation : ' + user['organisation'] + '\n\n' +
            'Email : ' + user['email'] + '\n\n' +
            'Phone Number : ' + user['mobile_number'] + '\n\n' +
            'Description  : ' + user['description'] + '\n\n' +
            'Regards';


        sql.getConnection((err, connection) => {
            if (err) {
                // connection.release();
                console.log(' Error getting mysql_pool connection: ' + err);
                return res.status(400).json({message: 'Oops! We are working on something!'});
            }


            connection.query("INSERT INTO users set ?", user, function (err, data) {

                mail.sendEmail(emailFromWeb, emailFrom, subContact, contentRequest);
                ejs.renderFile('contact.ejs', {name: user.name}, null, function (err, html) {
                    mail.sendEmail(emailFrom, user.email, subContactToUser, html, true);
                });

                connection.release();
                if (err) {
                    console.log(err);
                    // return sendRes(400, 'Oops! We are working on something!');
                    return res.status(400).json({message: 'Oops! We are working on something!'});
                } else {
                    // return sendRes(200, 'Success!');
                    return res.status(200).json({message: 'Success'});
                }
            });
        });
    } catch (exception) {
        console.log(exception);
        // return sendRes(400, 'Oops! We are working on something!');
        return res.status(400).json({message: 'Oops! We working on it!'});
    }
});

app.post('/request', function (req, res) {
    try {
        // sql.connect();
        console.log(req.body);

        const user = {
            name: req.body.name ? req.body.name : '',
            organisation: req.body.organisation ? req.body.organisation : '',
            mobile_number: req.body.mobile_number ? req.body.mobile_number : '',
            email: req.body.email ? req.body.email : '',
            description: req.body.description ? req.body.description : '',
            position: req.body.position ? req.body.position : '',
            fleet_size: req.body.fleet_size ? req.body.fleet_size : '',
            request_type: req.body.request_type ? req.body.request_type : '',
            status: 'Contacted',
            source: 'KonnectShift',
            requested_at: Moment.utc().format('YYYY-MM-DD HH:mm:ss')
        };

        const contentRequest = 'Hi Team, ' + user['name'] + '\n\n just requested a demo for KonnectShift.' + '\n\n' +
            'Name : ' + user['name'] + '\n\n' +
            'Organisation : ' + user['organisation'] + '\n\n' +
            'Email : ' + user['email'] + '\n\n' +
            'Phone Number : ' + user['mobile_number'] + '\n\n' +
            'Description  : ' + user['description'] + '\n\n' +
            'Regards';
        sql.getConnection((err, connection) => {
            if (err) {
                connection.release();
                console.log(' Error getting mysql_pool connection: ' + err);
                return res.status(400).json({message: 'Oops! We are working on something!'});
            }

            connection.query("INSERT INTO users set ?", user, function (err, data) {

                mail.sendEmail(emailFromWeb, emailFrom, subRequest, contentRequest);
                ejs.renderFile('request.ejs', {name: user.name}, null, function (err, html) {
                    // mail.sendEmail(emailFrom, user.email, subContactToUser, html, true);
                    mail.sendEmail(emailFrom, user.email, subRequestToUser, html, true);
                });

                connection.release();
                if (err) {
                    console.log(err);
                    // return sendRes(400, 'Oops! We are working on something!');
                    return res.status(400).json({message: 'Oops! We are working on something!'});
                } else {
                    // return sendRes(200, 'Success!');
                    return res.status(200).json({message: 'Success'});
                }
            });


        });


    } catch (exception) {
        console.log(exception);
        // return sendRes(400, 'Oops! We are working on something!');

        return res.status(400).json({message: 'Oops! We working on it!'});
    }
});

app.post('/careers', upload.single('resume'), function (req, res) {
    try {
        // sql.connect();
        console.log(req.body);

        const career = {
            name: req.body.name ? req.body.name : '',
            dob: req.body.dob ? req.body.dob : '',
            phone: req.body.phone ? req.body.phone : '',
            email: req.body.email ? req.body.email : '',
            linkedin: req.body.linkedin ? req.body.linkedin : '',
            description: req.body.desc ? req.body.desc : '',
            job_profile: req.body.job_profile ? req.body.job_profile : '',
            status: 'Contacted',
            source: 'KonnectShift',
            requested_at: Moment.utc().format('YYYY-MM-DD HH:mm:ss')
        };

        console.log(req.file.originalname);
        fs.readFile(req.file.path, (err, data) => {
            if (err) throw err;
            const params = {
                Bucket: 'konnect-parking-uploads',
                Body: data,
                ACL: 'public-read',
                Key: "fleetDev/" + Date.now() + req.file.originalname,
            };

            console.log("AWS");
            s3.upload(params, function (err, data) {
                fs.unlinkSync(req.file.path);
                //handle error
                if (err) {
                    return res.status(400).json({message: 'Unable to upload resume!'});
                }
                //success
                if (data) {
                    console.log("Uploaded in:", data.Location, data);
                    career.resume = data.Location;


                    const contentCareer = 'Hi Team, ' + career['name'] + '\n\n just contacted us for ' + career['job_profile'] + '  for KonnectShift.' + '\n\n' +
                        'Job Profile : ' + career['job_profile'] + '\n\n' +
                        'Name : ' + career['name'] + '\n\n' +
                        'Date of Birth : ' + career['dob'] + '\n\n' +
                        'Email : ' + career['email'] + '\n\n' +
                        'Phone Number : ' + career['phone'] + '\n\n' +
                        'Linked In : ' + career['linkedin'] + '\n\n' +
                        'Resume : ' + career['resume'] + '\n\n' +
                        'Description  : ' + career['description'] + '\n\n' +
                        'Regards';

                    sql.getConnection((err, connection) => {
                        if (err) {
                            connection.release();
                            console.log(' Error getting mysql_pool connection: ' + err);
                            return res.status(400).json({message: 'Oops! We are working on something!'});
                        }

                        connection.query("INSERT INTO careers set ?", career, function (err, data) {

                            mail.sendEmail(emailFromWeb, emailFrom, subCareer, contentCareer);
                            ejs.renderFile('request.ejs', {name: career.name}, null, function (err, html) {
                                // mail.sendEmail(emailFrom, user.email, subContactToUser, html, true);
                                mail.sendEmail(emailFrom, career.email, subRequestToUser, html, true);
                            });

                            connection.release();
                            if (err) {
                                console.log(err);
                                // return sendRes(400, 'Oops! We are working on something!');
                                return res.status(400).json({message: 'Oops! We are working on something!'});
                            } else {
                                // return sendRes(200, 'Success!');
                                return res.status(200).json({message: 'Success'});
                            }
                        });


                    });

                }
            });
        });
    } catch (exception) {
        console.log(exception);
        // return sendRes(400, 'Oops! We are working on something!');

        return res.status(400).json({message: 'Oops! We working on it!'});
    }
});

app.post('/shyftlabs', function (req, res) {
    try {

        console.log(req.body);

        const user = {
            name: req.body.name ? req.body.name : '',
            organisation: req.body.organisation ? req.body.organisation : '',
            mobile_number: req.body.mobile_number ? req.body.mobile_number : '',
            email: req.body.email ? req.body.email : '',
            description: req.body.description ? req.body.description : '',
            position: req.body.position ? req.body.position : '',
            fleet_size: req.body.fleet_size ? req.body.fleet_size : '',
            request_type: req.body.request_type ? req.body.request_type : '',
            status: 'Contacted',
            source: 'ShyftLabs',
            requested_at: Moment.utc().format('YYYY-MM-DD HH:mm:ss')
        };

        const contentRequest = 'Hi Team, ' + user['name'] + '\n\n just contacted us for ShyftLabs.' + '\n\n' +
            'Name : ' + user['name'] + '\n\n' +
            'Organisation : ' + user['organisation'] + '\n\n' +
            'Email : ' + user['email'] + '\n\n' +
            'Phone Number : ' + user['mobile_number'] + '\n\n' +
            'Description  : ' + user['description'] + '\n\n' +
            'Regards';


        sql.getConnection((err, connection) => {
            if (err) {
                connection.release();
                console.log(' Error getting mysql_pool connection: ' + err);
                return res.status(400).json({message: 'Oops! We are working on something!'});
            }


            connection.query("INSERT INTO users set ?", user, function (err, data) {

                mail.sendEmail(emailFromWebLabs, emailFromShyftLabs, subContactLabs, contentRequest);
                ejs.renderFile('contactLabs.ejs', {name: user.name}, null, function (err, html) {
                    mail.sendEmail(emailFromShyftLabs, user.email, subContactToUser, html, true);
                });

                connection.release();
                if (err) {
                    console.log(err);
                    // return sendRes(400, 'Oops! We are working on something!');
                    return res.status(400).json({message: 'Oops! We are working on something!'});
                } else {
                    // return sendRes(200, 'Success!');
                    return res.status(200).json({message: 'Success'});
                }
            });
        });
    } catch (exception) {
        console.log(exception);
        // return sendRes(400, 'Oops! We are working on something!');
        return res.status(400).json({message: 'Oops! We working on it!'});
    }
});
